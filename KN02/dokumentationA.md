Sowohl das konzeptionelle, als auch das Logische Model findet man in Repo unter MongoDB.png
![Alt text](image.png)

Die vier Entitäten sind:
 - Dinosaurier
 - Kategorie
 - Biom
 - Pflanze

Jeder Dinosaurier ist Teil genau einer Kategorie. In einer Kategorie kann es jedoch mehrere Dinosaurier haben.
Zusätzlich leben mehrere Dinosaurier in einem Biom oder mehreren Biomen. In einem Biom hat es auch mehrere Dinosaurier.
Pflanzen findet man immer nur in einem Biom, dafür findet man in einem Biom mehrer Pflanzen.