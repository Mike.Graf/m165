db.Category.updateOne({_id: catID1}, {$set: {"nutrition": "everything"}});

db.Dinosaurs.updateMany(
    { $or: [
        { color: "brown" },
        { color: "green" }
    ] },
    { $set: { color: "black" } }
);

db.Biom.replaceOne(
    { _id: biomID1 },
    {
        _id: biomID1,
        name: "rainforest",
        "avrg temperature": 30,
        "avrg moisture": 80,
        plants: [
            { plant_id: plantID1, name: "tree", height: 100, poisonous: false },
            { plant_id: plantID2, name: "bush", height: 50, poisonous: true },
            { plant_id: plantID3, name: "grass", height: 1, poisonous: false }
        ]
    }
);