var catID1 = new ObjectId();
var catID2 = new ObjectId();

var dinoID1 = new ObjectId();
var dinoID2 = new ObjectId();
var dinoID3 = new ObjectId();
var dinoID4 = new ObjectId();
var dinoID5 = new ObjectId();


db.Dinosaurs.insertOne({_id: dinoID1,   "name": "t-rex",
                                        "hight(m)": 13,
                                        "weight(kg)": 8800,
                                        "color": "green",
                                        "extinction-date": new Date("1011-01-01"),
                                        });

db.Dinosaurs.insertMany([{_id: dinoID2,   "name": "ankylosaurus",
                                        "hight(m)": 3,
                                        "weight(kg)": 8000,
                                        "color": "brown",
                                        "extinction-date": new Date("1012-01-01"),
                                        },
                        {_id: dinoID3,   "name": "stegosaurus",
                                        "hight(m)": 6.5,
                                        "weight(kg)": 5500,
                                        "color": "light green",
                                        "extinction-date": new Date("1013-01-01"),
                                        },
                        {_id: dinoID4,   "name": "carnotaurus",
                                         "hight(m)": 4.5,
                                         "weight(kg)": 3200,
                                         "color": "red-brown",
                                         "extinction-date": new Date("1014-01-01"),
                                        },
                        {_id: dinoID5,  "name": "triceraptops",
                                        "hight(m)": 4,
                                        "weight(kg)": 4000,
                                        "color": "grey",
                                        "extinction-date": new Date("1015-01-01"),
                                        }
                                    ]);
                                    
db.Category.insertMany([{_id: catID1,   "name": "carnivor",
                                        "nutrition": "meat",
                                    },
                        {_id: catID2,   "name": "herbivor",
                                        "nutrition": "plants",
                                    }
                                ]);    

var plantID1 = new ObjectId();
var plantID2 = new ObjectId();
var plantID3 = new ObjectId();

var biomID1 = new ObjectId();
var biomID2 = new ObjectId();
var biomID3 = new ObjectId();


db.Biom.insertMany([{_id: biomID1,      "name": "jungle",
                                        "avrg temperature": 25,
                                        "avrg moisture": 75,
                                        "plants": [ {   "plant_id": plantID1, "name": "tree",
                                                        "height": 100,
                                                        "poisonous": false },
                                                    {   "plant_id": plantID2, "name": "bush",
                                                        "height": 50,
                                                        "poisonous": true},
                                                    {   "plant_id": plantID3, "name": "grass",
                                                        "height": 1,
                                                        "poisonous": false}]
                                },
                    {_id: biomID2,      "name": "grassland",
                                        "avrg temperature": 16,
                                        "avrg moisture": 45,
                                        "plants": [ {   "plant_id": plantID3, "name": "grass",
                                                        "height": 1,
                                                        "poisonous": false}]
                                },
                    {_id: biomID3,      "name": "woods",
                                        "avrg temperature": 16,
                                        "avrg moisture": 65,
                                        "plants": [ {   "plant_id": plantID1, "name": "tree",
                                        "height": 100,
                                        "poisonous": false },
                                    {   "plant_id": plantID2, "name": "bush",
                                        "height": 50,
                                        "poisonous": true}]
                                }
                                ]); 

