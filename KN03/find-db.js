db.Dinosaurs.find();
db.Biom.find();
db.Category.find();

db.Dinosaurs.find({ "extinction-date": ISODate("1013-01-01") });

db.Dinosaurs.find({
    $or: [
        { color: "brown" },
        { "weight(kg)": { $gt: 5000 } },
        { name: "stegosaurus" }
    ]
});

db.Biom.find({ "avrg moisture": {$gt: 50}, "avrg temperature": 16});

db.Dinosaurs.find({name: /aurus/});

db.Dinosaurs.find({}, { name: 1, color: 1, _id: 1 });

db.Dinosaurs.find({}, { name: 1, color: 1, _id: 0 });

