- Bild der Fehlermeldung:
![alt text](image-19.png)

- Bild des funktionierenden Einloggens für Benutzer 1:
![alt text](image-1.png)

- Bild das Benutzer 1 lesen kann:
![alt text](image-2.png)

- Bild der fehlenden Schreibrechte für benutzer 1:
![alt text](image-3.png)

- Bild des funktionierenden Einloggens für Benutzer 2:
![alt text](image-4.png)

- Bild das Benutzer 2 lesen kann:
![alt text](image-6.png)

- Bild das Schreiben funktioniert:
![alt text](image-8.png)

- Bild des Validierungsbefehl:
![alt text](image-20.png)