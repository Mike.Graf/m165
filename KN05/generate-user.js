use test

db.createUser({
  user: "Benutzer1",
  pwd: "12345",
  roles: [
    { role: "read", db: "test" }
  ]
});

use admin

db.createUser({
  user: "Benutzer2",
  pwd: "12345",
  roles: [
    { role: "readWrite", db: "test" }
  ]
});