- Bild des Erstellten Snapshots:
![alt text](image-9.png)

- Bild das Collection Biom gelöscht wurde:
![alt text](image-10.png)

- Bild der Collection die wieder da ist und Status des neuen Volumens:
![alt text](image-11.png)
![alt text](image-12.png)

- Es wurden zur Erstellung des Snapshots, des Volumens und des Anfügen der Instanz an das Volumen keine Befehle verwendet. Ich habe alles auf der AWS-Seite per Klicks gemacht.

- sudo sudo mongodump --host localhost:27017 --username admin --password ###### --authenticationDatabase admin --db test --out C:temp/mongoDump

- Bild des Dump Status:
![alt text](image-13.png)

- Bild der gelöschten Collection:
![alt text](image-14.png)

- Bild des restore Status:
![alt text](image-15.png)

- Bild der wieder hergestellten Collection:
![alt text](image-16.png)

- sudo mongorestore --host localhost:27017 --username admin --password ####### --authenticationDatabase admin C:temp/mongoDump

