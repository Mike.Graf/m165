//a)
var actorID1 = new ObjectId();
var actorID2 = new ObjectId();
var actorID3 = new ObjectId();

db.movieDb.insertOne({_id: actorID1,    "name": "Reevs",
                                        "firstname": "Keanu",
                                        "age": 46,
                                        "rating": 9.0
                                        });

db.movieDb.insertOne({_id: actorID2,    "name": "Renolds",
                                        "firstname": "Rayen",
                                        "age": 40,
                                        "rating": 9.6
                                        });

db.movieDb.insertOne({_id: actorID3,    "name": "Freeman",
                                        "firstname": "Morgan",
                                        "age": 999,
                                        "rating": 8.0
                                        });

var movieID1 = new ObjectId();
var movieID2 = new ObjectId();

db.movieDb.insertMany([{_id: movieID1,  "titel": "super cool Dinosaurs",
                                        "director": {
                                            "name": "Baly",
                                            "firstname": "Christean"
                                        },
                                        "actors": [
                                            {
                                                "$oid": "actorID1"
                                            },
                                            {
                                                "$oid": "actorID2"
                                            }
                                        ],
                                         "length": 95,
                                         "released": {
                                            "$date": "2024-06-27T12:32"
                                        }
                                    },
                        {_id: movieID2,  "titel": "the Bee Movie",
                                        "director": {
                                            "name": "Bee",
                                            "firstname": "Honey"
                                        },
                                        "actors": [
                                            {
                                                "$oid": "actorID3"
                                            }
                                        ],
                                         "length": 95,
                                         "released": {
                                            "$date": "2024-06-27T12:32"
                                        }
                                    }])