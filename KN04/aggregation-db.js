//1)
db.Biom.aggregate([
    {
      $match: {
        "hight(m)": { $gt: 50 },
        "avrg temperature": 16
      }
    }
  ])

  //2)
  db.Dinosaurs.aggregate([
    {
      $match: {
        "hight(m)": { $gt: 4 },
        "weight(kg)": { $gt: 4000}
      }
    }
  ])

  db.Dinosaurs.aggregate([
    {
      $match: {
        "hight(m)": { $lt: 6.5 },
        "weight(kg)": { $lt: 8000}
      }
    },
    {
        $project: {
            "hight(m)": 1,
            "weight(kg)": 1,
            "name": { $cond: { if: { $lt: ["$hight(m)", 4.2] }, then: "small guy", else: "big guy"}
            }
        }
    }])

    db.Dinosaurs.aggregate([
        {
            $sort: { "hight(m)": 1 }
        }
      ])


//3)
db.Dinosaurs.aggregate([
    {
      $group: {
        _id: null,
        totalHeight: { $sum: "$hight(m)" }
      }
    }
  ])

//4)
db.Dinosaurs.aggregate([
    {
      $group: {
        _id: null,
        totalWeight: { $sum: "$weight(kg)" }
      }
    }
  ])

  