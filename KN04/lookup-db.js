db.Dinosaurs.aggregate([
    {
        $lookup: {
          from: "Category",           
          localField: "CatID",
          foreignField: "_id",         
          as: "DinoCategories"         
        }
      }
])

db.Dinosaurs.aggregate([
  {
      $lookup: {
        from: "Category",           
        localField: "CatID",
        foreignField: "_id",         
        as: "DinoCategories"         
      }
    },
    {
      $sort: { "Category": 1 }
    }
])
