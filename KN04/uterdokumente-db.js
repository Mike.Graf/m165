db.Biom.aggregate([
  {
      $project: {
          _id: 0,
          plants: "$plants"
      }
  }
])

db.Biom.aggregate([
  {
      $project: {
          _id: 0,
          poisonous_plants:{
            $filter: {
              input: "$plants",
              as: "plant",
              cond: { $eq: ["$$plant.poisonous", true] }
              }
            }
          }
    }
])

db.Biom.aggregate([
    { $unwind: "$plants" }
  ])

