#M165 Kompetenznachweise
KN 01
-	Siehe cloudinit-mongodb.yaml Datei.
-	Siehe Bild unten
 ![Alt text](image.png)
-Der Connection String Parameter authSource definiert welche Datenbank für die Authentifizierung verwendet wird. Da unsere Anmeldedaten in der admin Datenbank gespeichert sind, ist die Zuweisung authSource = admin korrekt.
-der sed Befehl wird zum Editieren von Text verwendet. In diesem Fall wird «#security:» durch «security:» ersetzt, dann wird eine Zeile eingefügt und dann der String «authorization: enabled» angehängt.
Zusätzlich wird weiter unten das 127.0.0.1 durch 0.0.0.0 ersetzt.

Wie löschen das # bei security, damit es nicht mehr als Kommentar angesehen wird. Somit ist der security-Block aktiviert. 
authorization: enabled sorgt dafür, dass sich Benutzer authentifizieren müssen und ihre Berechtigungen überprüft werden. 
Wir ersetzen die IP Adresse, damit man von allen Rechnern auf die DB zugreiffen kann und nicht nur vom LocalHost.

![Alt text](image-1.png)