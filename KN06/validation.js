db.runCommand({
    collMod: "Category",
    validator: {
      $jsonSchema: {
        bsonType: "object",
        required: ["_id", "name"],
        properties: {
          _id: {
            bsonType: "objectId",
            description: "must be an ObjectId and is required"
          },
          name: {
            bsonType: "string",
            description: "must be a string and is required"
          },
          nutrition: {
            bsonType: "string",
            description: "must be a string and is optional"
          }
        }
      }
    },
    validationLevel: "strict"
  })


  db.runCommand({
    collMod: "Biom",
    validator: {
        "$jsonSchema": {
          "bsonType": "object",
          "required": ["_id", "name", "plants"],
          "properties": {
            "_id": {
              "bsonType": "objectId",
              "description": "must be an ObjectId and is required"
            },
            "name": {
              "bsonType": "string",
              "description": "must be a string and is required"
            },
            "avrgTemperature": {
              "bsonType": "int",
              "description": "must be an integer and is optional"
            },
            "avrgMoisture": {
              "bsonType": "int",
              "description": "must be an integer and is optional"
            },
            "plants": {
              "bsonType": "array",
              "description": "must be an array of plant objects and is required",
              "items": {
                "bsonType": "object",
                "required": ["_id", "name"],
                "properties": {
                  "_id": {
                    "bsonType": "objectId",
                    "description": "must be an ObjectId and is required"
                  },
                  "name": {
                    "bsonType": "string",
                    "description": "must be a string and is required"
                  },
                  "height": {
                    "bsonType": "int",
                    "description": "must be an integer and is optional"
                  },
                  "poisonous": {
                    "bsonType": "bool",
                    "description": "must be a boolean and is optional"
                  }
                }
              }
            }
          }
        }
     }}
  )
  
  
  db.runCommand({
    collMod: "Dinosaurs",
    validator: {
        "$jsonSchema": {
          "bsonType": "object",
          "required": ["_id", "name", "extinctionDate"],
          "properties": {
            "_id": {
              "bsonType": "objectId",
              "description": "must be an ObjectId and is required"
            },
            "name": {
              "bsonType": "string",
              "description": "must be a string and is required"
            },
            "height": {
              "bsonType": "int",
              "description": "must be an integer and is optional"
            },
            "weight": {
              "bsonType": "int",
              "description": "must be an integer and is optional"
            },
            "color": {
              "bsonType": "string",
              "description": "must be a string and is optional"
            },
            "extinctionDate": {
              "bsonType": "date",
              "description": "must be a date and is required"
            }
          }
        }
      }
  })



  var catID3 = new ObjectId();

  db.Category.insertOne([{_id: catID3,
                                        "nutrition": "12",
                                    }])


db.getCollectionInfos({name: "Dinosaurs"})[0].options.validator;

